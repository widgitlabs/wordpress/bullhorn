<?php
/**
 * Helper functions
 *
 * @package     Bullhorn\Functions
 * @since       1.0.0
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Get the strings for post expiration
 *
 * @since       1.0.0
 * @param       int  $post_id The ID of the post.
 * @param       bool $short_date Whether or not to return short date format.
 * @param       bool $ampm Whether or not to return time in 24-hour format.
 * @return      array $strings The expiration strings.
 */
function bullhorn_get_expiration_strings( $post_id = 0, $short_date = false, $ampm = false ) {
	if ( ! $post_id ) {
		global $post;

		$post_id = $post->ID;
	}

	$label      = __( 'Expires', 'bullhorn' );
	$expiration = __( 'Never', 'bullhorn' );

	/* translators: Expiration date string. 1: Date, 2: Time. See https://www.php.net/manual/datetime.format.php */
	$date_string = __( '%1$s at %2$s', 'bullhorn' );
	/* translators: Expiration date format, see https://www.php.net/manual/datetime.format.php */
	$date_format = _x( 'M j, Y', 'expiration date format', 'bullhorn' );

	if ( $short_date ) {
		/* translators: Expiration short date format, see https://www.php.net/manual/datetime.format.php */
		$date_format = _x( 'Y/m/j', 'expiration short date format', 'bullhorn' );
	}

	/* translators: Expiration time format, see https://www.php.net/manual/datetime.format.php */
	$time_format = _x( 'H:i', 'expiration time format', 'bullhorn' );

	if ( $ampm ) {
		/* translators: Expiration time twelve-hour format, see https://www.php.net/manual/datetime.format.php */
		$time_format = _x( 'g:i a', 'expiration twelve-hour time format', 'bullhorn' );
	}

	if ( bullhorn_notification_expires( $post_id ) ) {
		$label = __( 'Expires on', 'bullhorn' );

		if ( bullhorn_post_expired( $post_id ) ) {
			$label = __( 'Expired on', 'bullhorn' );
		}

		$expiration = get_post_meta( $post_id, 'post_expiration', true );
		$expiration = get_date_from_gmt( $expiration );
		$expiration = sprintf(
			$date_string,
			date_i18n( $date_format, strtotime( $expiration ) ),
			date_i18n( $time_format, strtotime( $expiration ) )
		);
	}

	$strings = array(
		'label'      => $label,
		'expiration' => $expiration,
	);

	return apply_filters( 'bullhorn_expiration_strings', $strings, $post_id, $date_string, $date_format, $time_format );
}


/**
 * Check if a post expires.
 *
 * @since       1.0.0
 * @param       int $post_id The ID of the post to check.
 * @return      bool $expires True if expires, false otherwise.
 */
function bullhorn_notification_expires( $post_id = 0 ) {
	if ( ! $post_id ) {
		global $post;

		$post_id = $post->ID;
	}

	$expiration = get_post_meta( $post_id, 'post_expiration', true );
	$expires    = ( ! $expiration ) ? false : true;

	return $expires;
}


/**
 * Check if a post is expired.
 *
 * @since       1.0.0
 * @param       int $post_id The ID of the post to check.
 * @return      bool $expired True if expired, false otherwise.
 */
function bullhorn_notification_expired( $post_id = 0 ) {
	if ( ! $post_id ) {
		global $post;

		$post_id = $post->ID;
	}

	$expiration = get_post_meta( $post_id, 'post_expiration', true );
	$expired    = false;

	if ( $expiration && time() > strtotime( $expiration ) ) {
		$expired = true;
	}

	return $expired;
}


/**
 * Print out HTML form date elements for expiration date.
 *
 * @since       1.0.0
 * @param       int $tab_index The tabindex attribute to add. Default 0.
 * @return      void
 */
function bullhorn_touch_time( $tab_index = 0 ) {
	global $wp_locale;

	$post = get_post();

	$tab_index_attribute = '';

	if ( (int) $tab_index > 0 ) {
		$tab_index_attribute = ' tabindex="$tab_index"';
	}

	$jj = current_time( 'd' );
	$mm = current_time( 'm' );
	$aa = current_time( 'Y' );
	$hh = current_time( 'H' );
	$mn = current_time( 'i' );

	$expiration_date = get_post_meta( $post->ID, 'post_expiration', true );
	$local_date      = get_date_from_gmt( $expiration_date );

	if ( $local_date ) {
		$jj = mysql2date( 'd', $local_date, false );
		$mm = mysql2date( 'm', $local_date, false );
		$aa = mysql2date( 'Y', $local_date, false );
		$hh = mysql2date( 'H', $local_date, false );
		$mn = mysql2date( 'i', $local_date, false );
	}

	$month = '<label><span class="screen-reader-text">' . __( 'Month', 'bullhorn' ) . '</span><select class="form-required" id="expiration-mm" ' . $tab_index_attribute . ">\n";

	for ( $i = 1; $i < 13; $i++ ) {
		$monthnum  = zeroise( $i, 2 );
		$monthtext = $wp_locale->get_month_abbrev( $wp_locale->get_month( $i ) );
		$month    .= "\t\t\t" . '<option value="' . $monthnum . '" data-text="' . $monthtext . '" ' . selected( $monthnum, $mm, false ) . '>';
		/* translators: 1: Month number (01, 02, etc.), 2: Month abbreviation. */
		$month .= sprintf( __( '%1$s-%2$s', 'bullhorn' ), $monthnum, $monthtext ) . "</option>\n";
	}
	$month .= '</select></label>';

	$day    = '<label><span class="screen-reader-text">' . __( 'Day', 'bullhorn' ) . '</span><input type="text" id="expiration-jj" value="' . $jj . '" size="2" maxlength="2"' . $tab_index_attribute . ' autocomplete="off" class="form-required" /></label>';
	$year   = '<label><span class="screen-reader-text">' . __( 'Year', 'bullhorn' ) . '</span><input type="text" id="expiration-aa" value="' . $aa . '" size="4" maxlength="4"' . $tab_index_attribute . ' autocomplete="off" class="form-required" /></label>';
	$hour   = '<label><span class="screen-reader-text">' . __( 'Hour', 'bullhorn' ) . '</span><input type="text" id="expiration-hh" value="' . $hh . '" size="2" maxlength="2"' . $tab_index_attribute . ' autocomplete="off" class="form-required" /></label>';
	$minute = '<label><span class="screen-reader-text">' . __( 'Minute', 'bullhorn' ) . '</span><input type="text" id="expiration-mn" value="' . $mn . '" size="2" maxlength="2"' . $tab_index_attribute . ' autocomplete="off" class="form-required" /></label>';

	echo '<div class="expiration-timestamp-wrap">';

	$timestamp_form = sprintf(
		/* translators: 1: Month, 2: Day, 3: Year, 4: Hour, 5: Minute. */
		__( '%1$s %2$s, %3$s at %4$s:%5$s', 'bullhorn' ),
		$month,
		$day,
		$year,
		$hour,
		$minute
	);

	echo wp_kses( $timestamp_form, bullhorn_kses_allowed_html() );

	echo "\n\n";

	$map = array(
		'mm' => $mm,
		'jj' => $jj,
		'aa' => $aa,
		'hh' => $hh,
		'mn' => $mn,
	);

	foreach ( $map as $timeunit => $value ) {
		echo '<input type="hidden" id="hidden-expiration-' . esc_attr( $timeunit ) . '" value="' . esc_attr( $value ) . '" />' . "\n";
	}

	echo '<input type="hidden" id="post-expiration" name="post_expiration" value="' . esc_attr( $expiration_date ) . '" />' . "\n";

	echo '<p>';
	echo '<a href="#edit_expiration_timestamp" class="save-expiration-timestamp hide-if-no-js button">' . esc_html__( 'OK', 'bullhorn' ) . '</a>';
	echo '<a href="#edit_expiration_timestamp" class="cancel-expiration-timestamp hide-if-no-js button-cancel">' . esc_html__( 'Cancel', 'bullhorn' ) . '</a>';
	echo '<a href="#edit_expiration_timestamp" class="remove-expiration-timestamp hide-if-no-js button-cancel">' . esc_html__( 'Remove Expiration', 'bullhorn' ) . '</a>';
	echo '</p>';
}


/**
 * Allowed html for input fields
 *
 * @since       1.0.0
 * @return      array $allowed_tags The allowed tags.
 */
function bullhorn_kses_allowed_html() {
	$allowed_tags = wp_kses_allowed_html( 'post' );
	$new_tags     = array(
		'a'        => array(
			'data-uploader_button_text' => array(),
			'data-uploader_title'       => array(),
		),
		'option'   => array(
			'data-text' => array(),
			'selected'  => array(),
			'value'     => array(),
		),
		'select'   => array(
			'class'    => array(),
			'id'       => array(),
			'name'     => array(),
			'tabindex' => array(),
			'multiple' => array(),
		),
		'input'    => array(
			'autocomplete'  => array(),
			'id'            => array(),
			'class'         => array(),
			'maxlength'     => array(),
			'name'          => array(),
			'size'          => array(),
			'tabindex'      => array(),
			'type'          => array(),
			'value'         => array(),
			'min'           => array(),
			'max'           => array(),
			'step'          => array(),
			'placeholder'   => array(),
			'data-sitekey'  => array(),
			'data-callback' => array(),
			'checked'       => array(),
		),
		'form'     => array(
			'id'     => array(),
			'class'  => array(),
			'action' => array(),
			'method' => array(),
		),
		'textarea' => array(
			'id'          => array(),
			'class'       => array(),
			'name'        => array(),
			'size'        => array(),
			'tabindex'    => array(),
			'placeholder' => array(),
			'rows'        => array(),
			'cols'        => array(),
		),
		'label'    => array(
			'for' => array(),
		),
		'li'       => array(
			'class' => array(),
			'id'    => array(),
			'style' => array(),
		),
		'ol'       => array(
			'class' => array(),
			'id'    => array(),
			'style' => array(),
		),
		'ul'       => array(
			'class' => array(),
			'id'    => array(),
			'style' => array(),
		),
		'span'     => array(
			'class' => array(),
			'id'    => array(),
			'style' => array(),
		),
		'div'      => array(
			'class' => array(),
			'id'    => array(),
			'style' => array(),
		),
		'p'        => array(
			'class' => array(),
			'id'    => array(),
			'style' => array(),
		),
		'script'   => array(
			'src'   => array(),
			'async' => array(),
			'defer' => array(),
		),
		'style'    => array(
			'type' => array(),
		),
	);

	return $allowed_tags + $new_tags;
}


/**
 * Sanitizes a string key for the HTML Elements class
 *
 * @since       1.0.0
 * @param       string $key String key.
 * @return      string The sanitized key.
 */
function bullhorn_sanitize_key( $key ) {
	$raw_key = $key;
	$key     = preg_replace( '/[^a-zA-Z0-9_\-\.\:\/\[\]]/', '', $key );

	return apply_filters( 'bullhorn_sanitize_key', $key, $raw_key );
}


/**
 * Render a metabox tabs container
 *
 * @since       1.0.0
 * @param       array $tabs The tab data to use for generation.
 * @param       int   $post_id The current post ID.
 * @return      void
 */
function bullhorn_render_metabox_tabs( $tabs = array(), $post_id ) {
	if ( ! empty( $tabs ) && 0 < (int) $post_id ) {
		?>
		<div class="bullhorn-metabox-tabs">
			<ul>
				<?php foreach ( $tabs as $tab_id => $tab_data ) : ?>
					<li><a href="#<?php echo esc_attr( $tab_id ); ?>"><?php echo esc_html( $tab_data['label'] ); ?></a></li>
				<?php endforeach; ?>
			</ul>
			<?php foreach ( $tabs as $tab_id => $tab_data ) : ?>
				<div id="<?php echo esc_attr( $tab_id ); ?>" style="display: none"><?php do_action( $tab_data['callback'], $post_id ); ?></div>
			<?php endforeach; ?>
		</div>
		<?php
	}
}
