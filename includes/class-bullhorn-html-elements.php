<?php
/**
 * HTML element helper class
 *
 * @package     Bullhorn\HTML
 * @since       1.0.0
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


/**
 * Helper class for various HTML elements
 *
 * @since       1.0.0
 */
class Bullhorn_HTML_Elements {


	/**
	 * Renders an HTML checkbox
	 *
	 * @access      public
	 * @since       1.0.0
	 * @param       array $args Arguments for the checkbox.
	 * @return      string $output The enerated checkbox.
	 */
	public function checkbox( $args = array() ) {
		$defaults = array(
			'name'    => null,
			'current' => null,
			'class'   => 'bullhorn-checkbox',
			'label'   => isset( $label ) ? $label : null,
			'options' => array(
				'disabled' => false,
				'readonly' => false,
			),
		);

		$args = wp_parse_args( $args, $defaults );

		$class   = implode( ' ', array_map( 'sanitize_html_class', explode( ' ', $args['class'] ) ) );
		$options = '';
		$output  = '<div class="bullhorn-checkbox">';

		if ( ! empty( $args['options']['disabled'] ) ) {
			$options .= ' disabled="disabled"';
		} elseif ( ! empty( $args['options']['readonly'] ) ) {
			$options .= ' readonly';
		}

		if ( ! empty( $args['label'] ) ) {
			$output .= '<label class="bullhorn-label" for="' . bullhorn_sanitize_key( $args['id'] ) . '">' . esc_html( $args['label'] ) . '</label>';
		}

		$output .= '<input type="checkbox"' . esc_attr( $options ) . ' name="' . esc_attr( $args['name'] ) . '" id="' . esc_attr( $args['id'] ) . '" class="' . esc_attr( $class ) . ' ' . esc_attr( $args['name'] ) . '" ' . checked( 'on', $args['current'], false ) . ' />';
		$output .= '</div>';

		return $output;
	}


	/**
	 * Renders an HTML number field
	 *
	 * @access      public
	 * @since       1.0.0
	 * @param       array $args Arguments for the number field.
	 * @return      string The generated number field.
	 */
	public function number( $args = array() ) {
		$defaults = array(
			'id'          => '',
			'name'        => isset( $name ) ? $name : 'number',
			'value'       => isset( $value ) ? $value : '',
			'label'       => isset( $label ) ? $label : null,
			'desc'        => isset( $desc ) ? $desc : null,
			'placeholder' => false,
			'class'       => 'small-text',
			'disabled'    => false,
			'min'         => false,
			'max'         => false,
			'step'        => 1,
		);

		$args = wp_parse_args( $args, $defaults );

		$class       = implode( ' ', array_map( 'sanitize_html_class', explode( ' ', $args['class'] ) ) );
		$disabled    = '';
		$placeholder = '';
		$min         = '';
		$max         = '';
		$step        = '';
		$data        = '';

		if ( $args['disabled'] ) {
			$disabled = ' disabled="disabled"';
		}

		if ( is_numeric( $args['placeholder'] ) ) {
			$placeholder = ' placeholder=' . $args['placeholder'];
		}

		if ( is_numeric( $args['min'] ) ) {
			$min = ' min=' . $args['min'];
		}

		if ( is_numeric( $args['max'] ) ) {
			$max = ' max=' . $args['max'];
		}

		if ( is_numeric( $args['step'] ) ) {
			$step = ' step=' . $args['step'];
		}

		if ( ! empty( $args['data'] ) ) {
			foreach ( $args['data'] as $key => $value ) {
				$data .= 'data-' . bullhorn_sanitize_key( $key ) . '="' . esc_attr( $value ) . '" ';
			}
		}

		$output = '<span id="bullhorn-' . esc_attr( $args['id'] ) . '-wrap">';
		if ( ! empty( $args['label'] ) ) {
			$output .= '<label class="bullhorn-label" for="' . bullhorn_sanitize_key( $args['id'] ) . '">' . esc_html( $args['label'] ) . '</label>';
		}

		if ( ! empty( $args['desc'] ) ) {
			$output .= '<span class="bullhorn-description">' . esc_html( $args['desc'] ) . '</span>';
		}

		$atts = $disabled . $placeholder . $data . $min . $max . $step;

		$output .= '<input type="number" name="' . esc_attr( $args['name'] ) . '" id="' . esc_attr( $args['id'] ) . '" class="' . esc_attr( $class ) . '" value="' . esc_attr( $args['value'] ) . '"' . esc_html( $atts ) . ' />';

		$output .= '</span>';

		return $output;
	}


	/**
	 * Renders an HTML dropdown
	 *
	 * @access      public
	 * @since       1.0.0
	 * @param       array $args Arguments for the dropdown.
	 * @return      string $output The generated dropdown.
	 */
	public function select( $args = array() ) {
		$defaults = array(
			'options'          => array(),
			'name'             => null,
            'label'            => isset( $label ) ? $label : null,
			'class'            => '',
			'id'               => '',
			'selected'         => array(),
			'select2'          => false,
			'placeholder'      => null,
			'multiple'         => false,
			'show_option_all'  => _x( 'All', 'all dropdown items', 'bullhorn' ),
			'show_option_none' => _x( 'None', 'no dropdown items', 'bullhorn' ),
			'data'             => array(),
			'readonly'         => false,
			'disabled'         => false,
		);

		$args = wp_parse_args( $args, $defaults );

		$data_elements = '';
		$multiple      = '';
		$placeholder   = '';
		$readonly      = '';
		$disabled      = '';

		foreach ( $args['data'] as $key => $value ) {
			$data_elements .= ' data-' . esc_attr( $key ) . '="' . esc_attr( $value ) . '"';
		}

		if ( $args['multiple'] ) {
			$multiple = ' MULTIPLE';
		}

		if ( $args['select2'] ) {
			$args['class'] .= ' bullhorn-select2';
		}

		if ( $args['placeholder'] ) {
			$placeholder = $args['placeholder'];
		}

		if ( isset( $args['readonly'] ) && $args['readonly'] ) {
			$readonly = ' readonly="readonly"';
		}

		if ( isset( $args['disabled'] ) && $args['disabled'] ) {
			$disabled = ' disabled="disabled"';
		}

		$class = implode( ' ', array_map( 'sanitize_html_class', explode( ' ', $args['class'] ) ) );

        $output = '<span id="bullhorn-' . esc_attr( $args['id'] ) . '-wrap">';
		if ( ! empty( $args['label'] ) ) {
			$output .= '<label class="bullhorn-label" for="' . bullhorn_sanitize_key( $args['id'] ) . '">' . esc_html( $args['label'] ) . '</label>';
		}

		$output .= '<select' . esc_attr( $disabled ) . esc_attr( $readonly ) . ' name="' . esc_attr( $args['name'] ) . '" id="' . esc_attr( $args['id'] ) . '" class="bullhorn-select ' . esc_attr( $class ) . '"' . esc_attr( $multiple ) . ' data-placeholder="' . esc_attr( $placeholder ) . '"' . esc_attr( $data_elements ) . '>';

		if ( ! isset( $args['selected'] ) || ( is_array( $args['selected'] ) && empty( $args['selected'] ) ) || ! $args['selected'] ) {
			$selected = '';
		}

		if ( $args['show_option_all'] ) {
			if ( $args['multiple'] && ! empty( $args['selected'] ) ) {
				$selected = selected( true, in_array( 0, (array) $args['selected'] ), false );
			} elseif ( ! $args['multiple'] || empty( $args['selected'] ) ) {
				$selected = selected( $args['selected'], 0, false );
			}

			$output .= '<option value="all"' . $selected . '>' . esc_html( $args['show_option_all'] ) . '</option>';
		}

		if ( ! empty( $args['options'] ) ) {
			if ( $args['show_option_none'] ) {
				if ( $args['multiple'] ) {
					$selected = selected( true, in_array( -1, $args['selected'] ), false );
				} elseif ( isset( $args['selected'] ) && ! is_array( $args['selected'] ) && ! empty( $args['selected'] ) ) {
					$selected = selected( $args['selected'], -1, false );
				}

				$output .= '<option value="-1"' . $selected . '>' . esc_html( $args['show_option_none'] ) . '</option>';
			}

			foreach ( $args['options'] as $key => $option ) {
				if ( $args['multiple'] && is_array( $args['selected'] ) ) {
					$selected = selected( true, in_array( (string) $key, $args['selected'] ), false );
				} elseif ( isset( $args['selected'] ) && ! is_array( $args['selected'] ) ) {
					$selected = selected( $args['selected'], $key, false );
				}

				$output .= '<option value="' . esc_attr( $key ) . '"' . $selected . '>' . esc_html( $option ) . '</option>';
			}
		}

		$output .= '</select>';

        $output .= '</span>';

		return $output;
	}


	/**
	 * Renders an HTML text field
	 *
	 * @access      public
	 * @since       1.0.0
	 * @param       array $args Arguments for the text field.
	 * @return      string The generated text field.
	 */
	public function text( $args = array() ) {
		$defaults = array(
			'id'           => '',
			'name'         => isset( $name ) ? $name : 'text',
			'value'        => isset( $value ) ? $value : '',
			'label'        => isset( $label ) ? $label : null,
			'desc'         => isset( $desc ) ? $desc : null,
			'placeholder'  => false,
			'class'        => 'regular-text',
			'disabled'     => false,
			'autocomplete' => false,
			'data'         => false,
		);

		$args = wp_parse_args( $args, $defaults );

		$class        = implode( ' ', array_map( 'sanitize_html_class', explode( ' ', $args['class'] ) ) );
		$disabled     = '';
		$placeholder  = '';
		$autocomplete = '';
		$data         = '';

		if ( $args['disabled'] ) {
			$disabled = ' disabled="disabled"';
		}

		if ( $args['placeholder'] ) {
			$placeholder = ' placeholder="' . $args['placeholder'] . '"';
		}

		if ( $args['autocomplete'] ) {
			$autocomplete = ' autocomplete="' . $args['placeholder'] . '"';
		}

		if ( ! empty( $args['data'] ) ) {
			foreach ( $args['data'] as $key => $value ) {
				$data .= 'data-' . bullhorn_sanitize_key( $key ) . '="' . esc_attr( $value ) . '" ';
			}
		}

		$output = '<span id="' . esc_attr( $args['id'] ) . '-wrap">';

		if ( ! empty( $args['label'] ) ) {
			$output .= '<label class="bullhorn-label" for="' . bullhorn_sanitize_key( $args['id'] ) . '">' . esc_html( $args['label'] ) . '</label>';
		}

		$atts = $disabled . $placeholder . $autocomplete . $data;

		$output .= '<input type="text" name="' . esc_attr( $args['name'] ) . '" id="' . esc_attr( $args['id'] ) . '" class="' . esc_attr( $class ) . '" value="' . esc_attr( $args['value'] ) . '"' . esc_html( $atts ) . ' />';

        if ( ! empty( $args['desc'] ) ) {
			$output .= '<span class="bullhorn-description">' . esc_html( $args['desc'] ) . '</span>';
		}

		$output .= '</span>';

		return $output;
	}


	/**
	 * Renders an HTML textarea
	 *
	 * @access      public
	 * @since       1.0.0
	 * @param       array $args Arguments for the textarea.
	 * @return      string $output The generated textarea.
	 */
	public function textarea( $args = array() ) {
		$defaults = array(
			'id'       => '',
			'name'     => 'textarea',
			'value'    => null,
			'label'    => null,
			'desc'     => null,
			'class'    => 'large-text',
			'disabled' => false,
			'rows'     => 5,
		);

		$args = wp_parse_args( $args, $defaults );

		$class    = implode( ' ', array_map( 'sanitize_html_class', explode( ' ', $args['class'] ) ) );
		$disabled = '';

		if ( $args['disabled'] ) {
			$disabled = ' disabled="disabled"';
		}

		$output = '<span id="' . esc_attr( $args['id'] ) . '-wrap">';

		if ( ! empty( $args['label'] ) ) {
			$output .= '<label class="bullhorn-label" for="' . bullhorn_sanitize_key( $args['name'] ) . '">' . esc_html( $args['label'] ) . '</label>';
		}

		$output .= '<textarea name="' . esc_attr( $args['name'] ) . '" id="' . esc_attr( $args['id'] ) . '" class="' . $class . '" rows="' . esc_attr( $args['rows'] ) . '"' . $disabled . '>' . esc_html( $args['value'] ) . '</textarea>';

		if ( ! empty( $args['desc'] ) ) {
			$output .= '<span class="bullhorn-description">' . esc_html( $args['desc'] ) . '</span>';
		}

		$output .= '</span>';

		return $output;
	}


    /**
	 * Renders an editor
	 *
	 * @access      public
	 * @since       1.0.0
	 * @param       array $args Arguments passed by the setting.
	 * @return      void
	 */
	public function editor( $args ) {
		$defaults = array(
            'id'             => '',
            'name'           => 'textarea',
            'value'          => null,
            'label'          => null,
            'desc'           => null,
            'class'          => 'large-text',
            'disabled'       => false,
            'rows'           => 5,
            'buttons'        => true,
            'editor_html'    => true,
            'editor_tinymce' => true,
            'teeny'          => false,
            'wpautop'        => true,
		);

        $args = wp_parse_args( $args, $defaults );

		$class    = implode( ' ', array_map( 'sanitize_html_class', explode( ' ', $args['class'] ) ) );
		$disabled = '';

		if ( $args['disabled'] ) {
			$disabled = ' disabled="disabled"';
		}

		$output = '<span id="' . esc_attr( $args['id'] ) . '-wrap">';

		if ( ! empty( $args['label'] ) ) {
			$output .= '<label class="bullhorn-label" for="' . bullhorn_sanitize_key( $args['name'] ) . '">' . esc_html( $args['label'] ) . '</label>';
		}

        ob_start();

		wp_editor(
			stripslashes( $args['value'] ),
			esc_attr( $args['id'] ),
			array(
				'editor_class'   => esc_attr( $args['class'] ),
				'media_buttons'  => (bool) $args['buttons'],
				'quicktags'      => (bool) $args['editor_html'],
				'teeny'          => (bool) $args['teeny'],
				'textarea_name'  => esc_attr( $args['name'] ),
				'textarea_rows'  => absint( $args['rows'] ),
				'tinymce'        => (bool) $args['editor_tinymce'],
				'wpautop'        => (bool) $args['wpautop'],
			)
		);

        $output .= ob_get_clean();

        if ( ! empty( $args['desc'] ) ) {
			$output .= '<span class="bullhorn-description">' . esc_html( $args['desc'] ) . '</span>';
		}

		$output .= '</span>';

		return $output;
	}



	/**
	 * Renders an HTML dropdown of months
	 *
	 * @since       1.0.0
	 * @param       string $name Name attribute of the dropdown.
	 * @param       int    $selected Month to select automatically.
	 * @return      string $output The generated month dropdown.
	 */
	public function month_dropdown( $name = 'month', $selected = 0 ) {
		$month    = 1;
		$options  = array();
		$selected = empty( $selected ) ? gmdate( 'n' ) : $selected;

		while ( $month <= 12 ) {
			$options[ absint( $month ) ] = bullhorn_month_num_to_name( $month );
			$month++;
		}

		$output = $this->select(
			array(
				'name'             => $name,
				'selected'         => $selected,
				'options'          => $options,
				'show_option_all'  => false,
				'show_option_none' => false,
			)
		);

		return $output;
	}


	/**
	 * Renders an HTML dropdown of years
	 *
	 * @since       1.0.0
	 * @param       string $name Name attribute of the dropdown.
	 * @param       int    $selected Year to select automatically.
	 * @param       int    $years_before Number of years before the current year the dropdown should start with.
	 * @param       int    $years_after Number of years after the current year the dropdown should finish at.
	 * @return      string $output Year dropdown.
	 */
	public function year_dropdown( $name = 'year', $selected = 0, $years_before = 5, $years_after = 0 ) {
		$current    = gmdate( 'Y' );
		$start_year = $current - absint( $years_before );
		$end_year   = $current + absint( $years_after );
		$selected   = empty( $selected ) ? gmdate( 'Y' ) : $selected;
		$options    = array();

		while ( $start_year <= $end_year ) {
			$options[ absint( $start_year ) ] = $start_year;
			$start_year++;
		}

		$output = $this->select(
			array(
				'name'             => $name,
				'selected'         => $selected,
				'options'          => $options,
				'show_option_all'  => false,
				'show_option_none' => false,
			)
		);

		return $output;
	}


	/**
	 * Renders an HTML Dropdown of all raffle Categories
	 *
	 * @access      public
	 * @since       1.0.0
	 * @param       string $name Name attribute of the dropdown.
	 * @param       int    $selected Category to select automatically.
	 * @return      string $output Category dropdown.
	 */
	public function category_dropdown( $name = 'bullhorn_categories', $selected = 0 ) {
		$categories = get_terms( 'raffle_category', apply_filters( 'bullhorn_category_dropdown', array() ) );
		$options    = array();

		foreach ( $categories as $category ) {
			$options[ absint( $category->term_id ) ] = esc_html( $category->name );
		}

		$category_labels = bullhorn_get_taxonomy_labels( 'raffle_category' );

		$output = $this->select(
			array(
				'name'             => $name,
				'selected'         => $selected,
				'options'          => $options,
				'show_option_all'  => sprintf(
					/* Translators: The label for the show all categories option. 1: The plural name of the taxonomy */
					_x( 'All %s', 'plural: Example: "All Categories"', 'bullhorn' ),
					$category_labels['name']
				),
				'show_option_none' => false,
			)
		);

		return $output;
	}
}
