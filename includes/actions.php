<?php
/**
 * Actions
 *
 * @package     Bullhorn\Actions
 * @since       1.0.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}


function bullhorn_display_top_bar() {
    $notification_args = array(
        'numberposts' => -1,
        'post_type' => 'notification',
    );

    $notifications = get_posts( $notification_args );

    if ( 0 < count( $notifications ) ) {
        foreach ( $notifications as $notification ) {
            $type   = get_post_meta( $notification->ID, '_bullhorn_notification_type', true );

            if ( 'image' === $type ) {
                $banner = get_the_post_thumbnail_url( $notification, 'full' );
                $url    = get_post_meta( $notification->ID, '_bullhorn_notification_url', true );

                if ( $url ) {
                    $content = '<a href="' . esc_url( $url ) . '">';
                }

                if ( ! empty( $banner ) ) {
                    $content .= '<img src="' . $banner . '" />';
                }

                if ( $url ) {
                    $content .= '</a>';
                }
            } else {
                $content = get_post_meta( $notification->ID, '_bullhorn_notification_content', true );
            }
            
            echo '<div class="bullhorn-top-bar">';		
    		echo wp_kses_post( $content );
            echo '</div>';
        }
    }
}
add_action( 'wp_head', 'bullhorn_display_top_bar' );


/**
 * Add 'expired' to the post states array
 *
 * @since       1.0.0
 * @param       array  $post_states The states for this post.
 * @param       object $post The WordPress post object.
 * @return      array $post_states The states for this post.
 */
function bullhorn_post_states( $post_states, $post ) {
    if ( 'notification' === $post->post_type ) {
		$post_states[] = __( 'Expired', 'bullhorn' );
	}

	return $post_states;
}


/**
 * Change all the things when posts are expired
 *
 * @since       1.0.0
 * @param       object $post The WordPress post object.
 * @return      void
 */
function bullhorn_post_expiration( $post ) {
    if ( 'notification' === $post->post_type ) {
		if ( bullhorn_notification_expired( $post->ID ) ) {
			remove_action( 'save_post', 'bullhorn_save_notification' );

            wp_update_post(
                array(
                    'ID'          => $post->ID,
                    'post_status' => 'draft',
                )
            );

            add_action( 'save_post', 'bullhorn_save_notification' );

            return;
        }

		add_filter( 'display_post_states', 'bullhorn_post_states', 10, 2 );

        return;
	}

	remove_filter( 'the_title', 'bullhorn_the_expired_title', 10, 2 );
	remove_filter( 'post_class', 'bullhorn_expired_post_class' );
	remove_filter( 'display_post_states', 'bullhorn_post_states', 10, 2 );
}
add_action( 'the_post', 'bullhorn_post_expiration' );


/**
 * Reformat the post title when a notification is expired
 *
 * @since       1.0.0
 * @param       string $title The notification post title.
 * @param       int    $post_id The WordPress post ID.
 * @return      string $title The notification post title.
 */
function bullhorn_the_expired_title( $title = '', $post_id = 0 ) {
	global $post;

    if ( 'notification' === $post->post_type ) {
		$format = __( 'Expired: {title}', 'bullhorn' );
		$title  = str_replace( '{title}', $title, $format );
	}

	return $title;
}


/**
 * Add the expired class to the post when expired
 *
 * @since       1.0.0
 * @param       array $classes The post classes.
 * @return      array $classes The post classes.
 */
function bullhorn_expired_post_class( $classes ) {
	$classes[] = 'bullhorn-expired';

	return $classes;
}
