<?php
/**
 * Scripts
 *
 * @package     Bullhorn\Scripts
 * @since       1.0.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Load scripts
 *
 * @since       1.0.0
 * @return      void
 */
function bullhorn_scripts() {
	$css_url = BULLHORN_URL . 'assets/css/';

	// Use minified libraries if SCRIPT_DEBUG is turned off.
	$suffix = ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) ? '' : '.min';

    // Setup versioning for internal assets.
    $css_ver = gmdate( 'ymd-Gis', filemtime( BULLHORN_DIR . 'assets/css/bullhorn' . $suffix . '.css' ) );

	wp_register_style( 'bullhorn', $css_url . 'bullhorn' . $suffix . '.css', array(), BULLHORN_VER . '-' . $css_ver );
	wp_enqueue_style( 'bullhorn' );
}
add_action( 'wp_enqueue_scripts', 'bullhorn_scripts' );


/**
 * Load admin scripts
 *
 * @since       1.0.0
 * @return      void
 */
function bullhorn_admin_enqueue_scripts() {
	// Use minified libraries if SCRIPT_DEBUG is turned off.
	$suffix = ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) ? '' : '.min';

	wp_enqueue_script( 'jquery-ui-tabs' );
	wp_enqueue_script( 'jquery-ui-tooltip' );

	wp_register_style( 'bullhorn-datetimepicker', BULLHORN_URL . 'assets/js/datetimepicker/build/jquery.datetimepicker.min.css', array(), '2.5.21' );
	wp_enqueue_style( 'bullhorn-datetimepicker' );
	wp_enqueue_script( 'bullhorn-datetimepicker', BULLHORN_URL . 'assets/js/datetimepicker/build/jquery.datetimepicker.full.min.js', array( 'jquery' ), '2.5.21', true );

	// Setup versioning for internal assets.
	$css_ver = gmdate( 'ymd-Gis', filemtime( BULLHORN_DIR . 'assets/css/admin' . $suffix . '.css' ) );
	$js_ver  = gmdate( 'ymd-Gis', filemtime( BULLHORN_DIR . 'assets/js/admin' . $suffix . '.js' ) );

	wp_register_style( 'bullhorn', BULLHORN_URL . 'assets/css/admin' . $suffix . '.css', array(), BULLHORN_VER . '-' . $css_ver );
	wp_enqueue_style( 'bullhorn' );

	wp_enqueue_script( 'bullhorn', BULLHORN_URL . 'assets/js/admin' . $suffix . '.js', array( 'jquery-ui-tooltip', 'bullhorn-datetimepicker' ), BULLHORN_VER . '-' . $js_ver, true );
}
add_action( 'admin_enqueue_scripts', 'bullhorn_admin_enqueue_scripts' );
