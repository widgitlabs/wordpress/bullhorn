<?php
/**
 * Register settings
 *
 * @package     Bullhorn\Admin\Settings\Register
 * @since       1.0.0
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Setup the settings menu
 *
 * @since       1.0.0
 * @param       array $menu The default menu settings.
 * @return      array $menu Our defined settings
 */
function bullhorn_add_menu( $menu ) {
	$menu['type']       = 'submenu';
	$menu['parent']     = 'edit.php?post_type=notification';
	$menu['page_title'] = __( 'Bullhorn', 'bullhorn' );
	$menu['menu_title'] = __( 'Settings', 'bullhorn' );
	$menu['show_title'] = true;

	return $menu;
}
add_filter( 'bullhorn_menu', 'bullhorn_add_menu' );


/**
 * Define our settings tabs
 *
 * @since       1.0.0
 * @param       array $tabs The default tabs.
 * @return      array $tabs Our defined tabs
 */
function bullhorn_settings_tabs( $tabs ) {
	$tabs['core']    = __( 'Core', 'bullhorn' );
	$tabs['support'] = __( 'Support', 'bullhorn' );

	return $tabs;
}
add_filter( 'bullhorn_settings_tabs', 'bullhorn_settings_tabs' );


/**
 * Define settings sections
 *
 * @since       1.0.0
 * @param       array $sections The default sections.
 * @return      array $sections Our defined sections
 */
function bullhorn_registered_settings_sections( $sections ) {
	// TODO: Make Welcome! only show on initial install and updates.
	$sections = array(
		'core'    => array(
			'welcome' => __( 'Welcome!', 'bullhorn' ),
		),
		'support' => array(),
	);

	return $sections;
}
add_filter( 'bullhorn_registered_settings_sections', 'bullhorn_registered_settings_sections' );


/**
 * Disable save button on unsavable tabs
 *
 * @since       1.0.0
 * @return      array $tabs The updated tabs
 */
function bullhorn_define_unsavable_tabs() {
	$tabs = array( 'support' );

	return $tabs;
}
add_filter( 'bullhorn_unsavable_tabs', 'bullhorn_define_unsavable_tabs' );


/**
 * Disable save button on unsavable sections
 *
 * @since       1.0.0
 * @return      array $tabs The updated tabs
 */
function bullhorn_define_unsavable_sections() {
	$sections = array( 'core/welcome' );

	return $sections;
}
add_filter( 'bullhorn_unsavable_sections', 'bullhorn_define_unsavable_sections' );


/**
 * Define our settings
 *
 * @since       1.0.0
 * @param       array $settings The default settings.
 * @return      array $settings Our defined settings
 */
function bullhorn_registered_settings( $settings ) {
	$core_settings = array(
		'core'    => apply_filters( 'bullhorn_registered_settings_core',
			array(
				'welcome' => array(
					array(
						'id'   => 'welcome_header',
						'name' => '<h2>' . __( 'Welcome to Bullhorn!', 'bullhorn' ) . '</h2>',
						'desc' => '',
						'type' => 'header',
					),
				),
			),
		),
	);

	$core_settings = apply_filters( 'bullhorn_registered_settings_modules', $core_settings );

	$support_settings = array(
		'support' => array(
			array(
				'id'   => 'support_header',
				'name' => __( 'Bullhorn Support', 'bullhorn' ),
				'desc' => '',
				'type' => 'header',
			),
			array(
				'id'   => 'system_info',
				'name' => __( 'System Info', 'bullhorn' ),
				'desc' => '',
				'type' => 'sysinfo',
			),
		),
	);

	return array_merge( $settings, $core_settings, $support_settings );
}
add_filter( 'bullhorn_registered_settings', 'bullhorn_registered_settings' );
