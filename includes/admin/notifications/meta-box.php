<?php
/**
 * Meta box
 *
 * @package     Bullhorn\Notifications\Meta_Box
 * @since       2.0.0
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Register the notification options meta box
 *
 * @since       1.0.0
 * @return      void
 */
function bullhorn_add_meta_boxes() {
	add_meta_box( 'bullhorn_notification_options', __( 'Notification Options', 'bullhorn' ), 'bullhorn_render_notification_options_meta_box', 'notification', 'normal', 'high' );
}
add_action( 'add_meta_boxes', 'bullhorn_add_meta_boxes' );


/**
 * Render notification options meta box
 *
 * @since       1.0.0
 * @global      object $post The post we are editing
 * @return      void
 */
function bullhorn_render_notification_options_meta_box() {
	global $post;

	$tabs = apply_filters(
		'bullhorn_meta_box_options_tabs',
		array(
			'content'  => array(
				'label'    => esc_html__( 'Content', 'bullhorn' ),
				'callback' => 'bullhorn_meta_box_notification_options_content_fields',
			),
			// 'style'    => array(
			// 	'label'    => esc_html__( 'Style', 'bullhorn' ),
			// 	'callback' => 'bullhorn_meta_box_notification_options_style_fields',
			// ),
		)
	);

	bullhorn_render_metabox_tabs( $tabs, $post->ID );

	// Allow extension of the meta box.
	do_action( 'bullhorn_notification_options_meta_box_fields', $post_id );

	wp_nonce_field( basename( __FILE__ ), 'bullhorn_meta_box_notification_options_nonce' );
}


/**
 * Notification Type
 *
 * @since       1.0.0
 * @param       int $post_id The WordPress post ID.
 * @return      void
 */
function bullhorn_meta_box_notification_options_field_type( $post_id = 0 ) {
	$type = get_post_meta( $post_id, '_bullhorn_notification_type', true );

	$field = Bullhorn()->html->select(
		array(
			'id'               => 'bullhorn-notification-type',
			'name'             => '_bullhorn_notification_type',
			'class'            => 'widefat bullhorn-field',
			'label'            => esc_attr__( 'Notification Type:', 'bullhorn' ),
			'selected'         => $type,
			'show_option_all'  => false,
			'show_option_none' => false,
			'options'          => array(
				'text'  => __( 'Text', 'bullhorn' ),
				'image' => __( 'Image', 'bullhorn' ),
			),
		),
	);

	echo wp_kses( $field, bullhorn_kses_allowed_html() );
}
add_action( 'bullhorn_meta_box_notification_options_content_fields', 'bullhorn_meta_box_notification_options_field_type', 10 );


/**
 * Notification Content
 *
 * @since       1.0.0
 * @param       int $post_id The WordPress post ID.
 * @return      void
 */
function bullhorn_meta_box_notification_options_field_content( $post_id = 0 ) {
	$url = get_post_meta( $post_id, '_bullhorn_notification_content', true );

	$field = Bullhorn()->html->textarea(
		array(
			'id'    => 'bullhorn-notification-content',
			'name'  => '_bullhorn_notification_content',
			'class' => 'widefat bullhorn-field',
			'label' => esc_attr__( 'Notification Content:', 'bullhorn' ),
			'value' => $url,
			'rows'  => 10,
		),
	);

	echo wp_kses( $field, bullhorn_kses_allowed_html() );
}
add_action( 'bullhorn_meta_box_notification_options_content_fields', 'bullhorn_meta_box_notification_options_field_content', 10 );


/**
 * Notification URL
 *
 * @since       1.0.0
 * @param       int $post_id The WordPress post ID.
 * @return      void
 */
function bullhorn_meta_box_notification_options_field_url( $post_id = 0 ) {
	$url = get_post_meta( $post_id, '_bullhorn_notification_url', true );

	$field = Bullhorn()->html->text(
		array(
			'id'    => 'bullhorn-notification-url',
			'name'  => '_bullhorn_notification_url',
			'class' => 'widefat bullhorn-field',
			'label' => esc_attr__( 'Notification URL:', 'bullhorn' ),
			'value' => $url,
			'desc'  => __( 'Set notification image through the featured image box at right.', 'bullhorn' ),
		),
	);

	echo wp_kses( $field, bullhorn_kses_allowed_html() );
}
add_action( 'bullhorn_meta_box_notification_options_content_fields', 'bullhorn_meta_box_notification_options_field_url', 10 );


/**
 * Save post meta when the save_post action is called
 *
 * @since       1.0.0
 * @param       int $post_id The ID of the post we are saving.
 * @global      object $post The post we are saving
 * @return      void
 */
function bullhorn_notification_options_meta_box_save( $post_id ) {
	global $post;

	// Don't process if nonce can't be validated.
	if ( ! isset( $_POST['bullhorn_meta_box_notification_options_nonce'] ) || ! wp_verify_nonce( wp_strip_all_tags( wp_unslash( $_POST['bullhorn_meta_box_notification_options_nonce'] ) ), basename( __FILE__ ) ) ) {
		return;
	}

	// Don't process if this is an auto-save.
	if ( ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) || ( defined( 'DOING_AJAX' ) && DOING_AJAX ) || isset( $_REQUEST['bulk_edit'] ) ) {
		return;
	}

	// Don't process if this is a revision.
	if ( isset( $post->post_type ) && 'revision' === $post->post_type ) {
		return;
	}

	// The default fields that get saved.
	$fields = apply_filters(
		'bullhorn_notification_options_meta_box_fields_save',
		array(
			'_bullhorn_notification_type',
			'_bullhorn_notification_content',
			'_bullhorn_notification_image',
			'_bullhorn_notification_url',
		)
	);

	foreach ( $fields as $field ) {
		if ( array_key_exists( $field, $_POST ) && isset( $_POST[ $field ] ) ) {
			$field      = wp_unslash( $field );
			$post_field = sanitize_text_field( wp_unslash( $_POST[ $field ] ) );

			if ( is_string( $post_field ) ) {
				$new = esc_attr( $post_field );
			} else {
				$new = $post_field;
			}

			$new = apply_filters( 'bullhorn_notification_options_meta_box_save_' . $field, $new );

			update_post_meta( $post_id, $field, $new );
		} else {
			delete_post_meta( $post_id, $field );
		}
	}
}
add_action( 'save_post', 'bullhorn_notification_options_meta_box_save' );


/**
 * Add expiration support to notifications
 *
 * @since       1.0.0
 * @return      void
 */
function bullhorn_add_post_expiration() {
	global $post;

	$post_id          = (int) $post->ID;
	$post_type        = $post->post_type;
	$post_type_object = get_post_type_object( $post_type );
	$can_publish      = current_user_can( $post_type_object->cap->publish_posts );

	// Contributors don't get to choose the date of publish.
	if ( $can_publish ) {
		?>
		<div class="misc-pub-section misc-pub-expiration">
			<span id="expiration-timestamp">
				<?php
				$strings = bullhorn_get_expiration_strings( $post_id );

				echo esc_html( $strings['label'] ) . ': <span>' . esc_html( $strings['expiration'] ) . '</span>';
				?>
			</span>
			<a href="#edit_expiration" class="edit-expiration-timestamp hide-if-no-js" role="button">
				<span aria-hidden="true"><?php esc_html_e( 'Edit', 'bullhorn' ); ?></span>
				<span class="screen-reader-text"><?php esc_html_e( 'Edit expiration date and time', 'bullhorn' ); ?></span>
			</a>
			<fieldset id="bullhorn-expiration-wrap" class="hide-if-js">
				<legend class="screen-reader-text"><?php esc_html_e( 'Expiration date and time', 'bullhorn' ); ?></legend>
				<?php bullhorn_touch_time( 1 ); ?>
			</fieldset>
		</div>
		<?php
		wp_nonce_field( basename( __FILE__ ), 'bullhorn_expiration_meta_box_nonce' );
	}
}
add_action( 'post_submitbox_misc_actions', 'bullhorn_add_post_expiration', 9 );
