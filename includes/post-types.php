<?php
/**
 * Post Type Functions
 *
 * @package     Bullhorn\Post_Types
 * @since       1.0.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Registers and sets up the notifications custom post type
 *
 * @since       1.0.0
 * @return      void
 */
function bullhorn_setup_post_types() {
	$notification_labels = apply_filters( 'bullhorn_notification_labels', array(
		'name'                  => _x( '%2$s', 'notification post type name', 'bullhorn' ),
		'singular_name'         => _x( '%1$s', 'singular notification post type name', 'bullhorn' ),
		'add_new'               => __( 'Add New', 'bullhorn' ),
		'add_new_item'          => __( 'Add New %1$s', 'bullhorn' ),
		'edit_item'             => __( 'Edit %1$s', 'bullhorn' ),
		'new_item'              => __( 'New %1$s', 'bullhorn' ),
		'all_items'             => __( 'All %2$s', 'bullhorn' ),
		'view_item'             => __( 'View %1$s', 'bullhorn' ),
		'search_items'          => __( 'Search %2$s', 'bullhorn' ),
		'not_found'             => __( 'No %2$s found', 'bullhorn' ),
		'not_found_in_trash'    => __( 'No %2$s found in Trash', 'bullhorn' ),
		'parent_item_colon'     => '',
		'menu_name'             => _x( '%2$s', 'notification post type menu name', 'bullhorn' ),
		'featured_image'        => __( '%1$s Image', 'bullhorn' ),
		'set_featured_image'    => __( 'Set %1$s Image', 'bullhorn' ),
		'remove_featured_image' => __( 'Remove %1$s Image', 'bullhorn' ),
		'use_featured_image'    => __( 'Use as %1$s Image', 'bullhorn' ),
		'attributes'            => __( '%1$s Attributes', 'bullhorn' ),
		'filter_items_list'     => __( 'Filter %2$s list', 'bullhorn' ),
		'items_list_navigation' => __( '%2$s list navigation', 'bullhorn' ),
		'items_list'            => __( '%2$s list', 'bullhorn' ),
	) );

	foreach ( $notification_labels as $key => $value ) {
		$notification_labels[ $key ] = sprintf( $value, bullhorn_get_label_singular(), bullhorn_get_label_plural() );
	}

	$notification_args = array(
		'labels'             => $notification_labels,
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'menu_icon'          => 'dashicons-megaphone',
		'rewrite'            => true,
		'capability_type'    => 'page',
		'map_meta_cap'       => true,
		'has_archive'        => true,
		'hierarchical'       => false,
		'supports'           => apply_filters( 'bullhorn_notification_supports', array( 'title', 'thumbnail', 'revisions', 'author' ) ),
	);
	register_post_type( 'notification', apply_filters( 'bullhorn_notification_post_type_args', $notification_args ) );
}
add_action( 'init', 'bullhorn_setup_post_types', 1 );

/**
 * Get Default Labels
 *
 * @since       1.0.0
 * @return      array $defaults Default labels
 */
function bullhorn_get_default_labels() {
	$defaults = array(
	   'singular' => __( 'Notification', 'bullhorn' ),
	   'plural'   => __( 'Notifications','bullhorn' )
	);

	return apply_filters( 'bullhorn_default_notification_name', $defaults );
}

/**
 * Get Singular Label
 *
 * @since       1.0.0
 * @param       bool $lowercase Whether or not to return lowercase.
 * @return      string $defaults['singular'] Singular label
 */
function bullhorn_get_label_singular( $lowercase = false ) {
	$defaults = bullhorn_get_default_labels();

	return ($lowercase) ? strtolower( $defaults['singular'] ) : $defaults['singular'];
}

/**
 * Get Plural Label
 *
 * @since       1.0.0
 * @return      string $defaults['plural'] Plural label
 */
function bullhorn_get_label_plural( $lowercase = false ) {
	$defaults = bullhorn_get_default_labels();

	return ( $lowercase ) ? strtolower( $defaults['plural'] ) : $defaults['plural'];
}

/**
 * Change default "Enter title here" input
 *
 * @since       1.0.0
 * @param       string $title Default title placeholder text
 * @return      string $title New placeholder text
 */
function bullhorn_change_default_title( $title ) {
	// If a frontend plugin uses this filter (check extensions before changing this function)
	if ( ! is_admin() ) {
		$label = bullhorn_get_label_singular();
		$title = sprintf( __( 'Enter %s name here', 'bullhorn' ), $label );

		return $title;
	}

	$screen = get_current_screen();

	if ( 'notification' == $screen->post_type ) {
		$label = bullhorn_get_label_singular();
		$title = sprintf( __( 'Enter %s name here', 'bullhorn' ), $label );
	}

	return $title;
}
add_filter( 'enter_title_here', 'bullhorn_change_default_title' );

/**
 * Updated Messages
 *
 * @since       1.0.0
 * @param       array $messages Post updated message
 * @return      array $messages New post updated messages
 */
function bullhorn_updated_messages( $messages ) {
	global $post, $post_ID;

	$url1 = '<a href="' . get_permalink( $post_ID ) . '">';
	$url2 = bullhorn_get_label_singular();
	$url3 = '</a>';

	$messages['download'] = array(
		1 => sprintf( __( '%2$s updated. %1$sView %2$s%3$s.', 'bullhorn' ), $url1, $url2, $url3 ),
		4 => sprintf( __( '%2$s updated. %1$sView %2$s%3$s.', 'bullhorn' ), $url1, $url2, $url3 ),
		6 => sprintf( __( '%2$s published. %1$sView %2$s%3$s.', 'bullhorn' ), $url1, $url2, $url3 ),
		7 => sprintf( __( '%2$s saved. %1$sView %2$s%3$s.', 'bullhorn' ), $url1, $url2, $url3 ),
		8 => sprintf( __( '%2$s submitted. %1$sView %2$s%3$s.', 'bullhorn' ), $url1, $url2, $url3 )
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'bullhorn_updated_messages' );

/**
 * Updated bulk messages
 *
 * @since       1.0.0
 * @param       array $bulk_messages Post updated messages
 * @param       array $bulk_counts Post counts
 * @return      array $bulk_messages New post updated messages
 */
function bullhorn_bulk_updated_messages( $bulk_messages, $bulk_counts ) {
	$singular = bullhorn_get_label_singular();
	$plural   = bullhorn_get_label_plural();

	$bulk_messages['notification'] = array(
		'updated'   => sprintf( _n( '%1$s %2$s updated.', '%1$s %3$s updated.', $bulk_counts['updated'], 'bullhorn' ), $bulk_counts['updated'], $singular, $plural ),
		'locked'    => sprintf( _n( '%1$s %2$s not updated, somebody is editing it.', '%1$s %3$s not updated, somebody is editing them.', $bulk_counts['locked'], 'bullhorn' ), $bulk_counts['locked'], $singular, $plural ),
		'deleted'   => sprintf( _n( '%1$s %2$s permanently deleted.', '%1$s %3$s permanently deleted.', $bulk_counts['deleted'], 'bullhorn' ), $bulk_counts['deleted'], $singular, $plural ),
		'trashed'   => sprintf( _n( '%1$s %2$s moved to the Trash.', '%1$s %3$s moved to the Trash.', $bulk_counts['trashed'], 'bullhorn' ), $bulk_counts['trashed'], $singular, $plural ),
		'untrashed' => sprintf( _n( '%1$s %2$s restored from the Trash.', '%1$s %3$s restored from the Trash.', $bulk_counts['untrashed'], 'bullhorn' ), $bulk_counts['untrashed'], $singular, $plural )
	);

	return $bulk_messages;
}
add_filter( 'bulk_post_updated_messages', 'bullhorn_bulk_updated_messages', 10, 2 );

/**
 * Add row actions for the notification custom post type
 *
 * @since       1.0.0
 * @param       array $actions
 * @param       WP_Post $post
 * @return      array
 */
function  bullhorn_notification_row_actions( $actions, $post ) {
	if ( 'notification' === $post->post_type ) {
		return array_merge( array( 'id' => 'ID: ' . $post->ID ), $actions );
	}

	return $actions;
}
add_filter( 'post_row_actions', 'bullhorn_notification_row_actions', 2, 100 );


/**
 * Register custom post statuses
 *
 * @since       1.0.0
 * @return      void
 */
function bullhorn_register_post_statuses() {
	register_post_status(
		'notification_expired',
		array(
			'label'                     => _x( 'Expired', 'Expired post status', 'label' ),
			'public'                    => true,
			'exclude_from_search'       => false,
			'show_in_admin_all_list'    => true,
			'show_in_admin_status_list' => true,
			/* Translators: The expired post status label. 1: Singular count label 2: Plural count label. */
			'label_count'               => _n_noop( 'Expired <span class="count">(%s)</span>', 'Expired <span class="count">(%s)</span>', 'bullhorn' ),
		)
	);
}
add_action( 'init', 'bullhorn_register_post_statuses', 2 );