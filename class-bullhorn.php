<?php
/**
 * Bullhorn
 *
 * @package     Bullhorn
 * @since       1.0.0
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'Bullhorn' ) ) {

	/**
	 * Main Bullhorn class
	 *
	 * @access      public
	 * @since       1.0.0
	 */
	final class Bullhorn {

		/**
		 * The one true Bullhorn
		 *
		 * @access      private
		 * @since       1.0.0
		 * @var         Bullhorn $instance The one true Bullhorn
		 */
		private static $instance;

		/**
		 * The settings object
		 *
		 * @access      public
		 * @since       1.0.0
		 * @var         object $settings The settings object
		 */
		public $settings;

		/**
		 * The HTML helper object
		 *
		 * @access      public
		 * @since       1.0.0
		 * @var         Bullhorn_HTML_Elements $html The HTML elements helper.
		 */
		public $html;

		/**
		 * The modules object
		 *
		 * @access      public
		 * @since       1.0.0
		 * @var         object $modules The modules object
		 */
		public $modules;

		/**
		 * Get active instance
		 *
		 * @access      public
		 * @since       1.0.0
		 * @static
		 * @return      object self::$instance The one true Bullhorn
		 */
		public static function instance() {
			if ( ! isset( self::$instance ) && ! ( self::$instance instanceof Bullhorn ) ) {
				self::$instance = new Bullhorn();
				self::$instance->setup_constants();
				self::$instance->includes();
				self::$instance->html = new Bullhorn_HTML_Elements();
				self::$instance->hooks();
			}

			return self::$instance;
		}

		/**
		 * Throw error on object clone
		 *
		 * The whole idea of the singleton design pattern is that there is
		 * a single object. Therefore, we don't want the object to be cloned.
		 *
		 * @access      protected
		 * @since       1.0.0
		 * @return      void
		 */
		public function __clone() {
			_doing_it_wrong( __FUNCTION__, esc_attr__( 'Cheatin&#8217; huh?', 'bullhorn' ), '1.0.0' );
		}

		/**
		 * Disable unserializing of the class
		 *
		 * @access      protected
		 * @since       1.0.0
		 * @return      void
		 */
		public function __wakeup() {
			_doing_it_wrong( __FUNCTION__, esc_attr__( 'Cheatin&#8217; huh?', 'bullhorn' ), '1.0.0' );
		}

		/**
		 * Setup plugin constants
		 *
		 * @access      private
		 * @since       1.0.0
		 * @return      void
		 */
		private function setup_constants() {
			// Plugin version.
			if ( ! defined( 'BULLHORN_VER' ) ) {
				define( 'BULLHORN_VER', '1.0.0' );
			}

			// Plugin path.
			if ( ! defined( 'BULLHORN_DIR' ) ) {
				define( 'BULLHORN_DIR', plugin_dir_path( __FILE__ ) );
			}

			// Plugin URL.
			if ( ! defined( 'BULLHORN_URL' ) ) {
				define( 'BULLHORN_URL', plugin_dir_url( __FILE__ ) );
			}

			// Plugin file.
			if ( ! defined( 'BULLHORN_FILE' ) ) {
				define( 'BULLHORN_FILE', __FILE__ );
			}
		}

		/**
		 * Run plugin base hooks
		 *
		 * @access      private
		 * @since       1.0.0
		 * @return      void
		 */
		private function hooks() {
			add_action( 'plugins_loaded', array( self::$instance, 'load_textdomain' ) );
		}

		/**
		 * Include necessary files
		 *
		 * @access      private
		 * @since       1.0.0
		 * @return      void
		 */
		private function includes() {
			global $bullhorn_options;

			// Conditionally load the settings library.
			if ( ! class_exists( 'Simple_Settings' ) ) {
				require_once BULLHORN_DIR . 'vendor/widgitlabs/simple-settings/class-simple-settings.php';
			}

			require_once BULLHORN_DIR . 'includes/admin/settings/register.php';

			self::$instance->settings = new Simple_Settings( 'bullhorn', 'core', array( 'sysinfo' ) );
			$bullhorn_options         = self::$instance->settings->get_settings();

			require_once BULLHORN_DIR . 'includes/actions.php';
			require_once BULLHORN_DIR . 'includes/class-bullhorn-html-elements.php';
			require_once BULLHORN_DIR . 'includes/misc-functions.php';
			require_once BULLHORN_DIR . 'includes/post-types.php';
			require_once BULLHORN_DIR . 'includes/scripts.php';

			if ( is_admin() ) {
				require_once BULLHORN_DIR . 'includes/admin/notifications/meta-box.php';
			}
		}

		/**
		 * Load plugin language files
		 *
		 * @access      public
		 * @since       1.0.0
		 * @return      void
		 */
		public function load_textdomain() {
			// Set filter for language directory.
			$lang_dir = dirname( plugin_basename( __FILE__ ) ) . '/languages/';
			$lang_dir = apply_filters( 'bullhorn_languages_directory', $lang_dir );

			// WordPress plugin locale filter.
			$locale = apply_filters( 'plugin_locale', get_locale(), 'bullhorn' );
			$mofile = sprintf( '%1$s-%2$s.mo', 'bullhorn', $locale );

			// Setup paths to current locale file.
			$mofile_local  = $lang_dir . $mofile;
			$mofile_global = WP_LANG_DIR . '/bullhorn/' . $mofile;
			$mofile_core   = WP_LANG_DIR . '/plugins/bullhorn/' . $mofile;

			if ( file_exists( $mofile_global ) ) {
				// Look in global /wp-content/languages/bullhorn folder.
				load_textdomain( 'bullhorn', $mofile_global );
			} elseif ( file_exists( $mofile_local ) ) {
				// Look in local /wp-content/plugins/bullhorn/languages/ folder.
				load_textdomain( 'bullhorn', $mofile_local );
			} elseif ( file_exists( $mofile_core ) ) {
				// Look in core /wp-content/languages/plugins/bullhorn/ folder.
				load_textdomain( 'bullhorn', $mofile_core );
			} else {
				// Load the default language files.
				load_plugin_textdomain( 'bullhorn', false, $lang_dir );
			}
		}
	}
}

/**
 * The main function responsible for returning the one true Bullhorn
 * instance to functions everywhere.
 *
 * Use this function like you would a global variable, except without
 * needing to declare the global.
 *
 * Example: <?php $bullhorn = bullhorn(); ?>
 *
 * @since       1.0.0
 * @return      Bullhorn The one true Bullhorn
 */
function bullhorn() {
	return Bullhorn::instance();
}

// Get things started.
bullhorn();
