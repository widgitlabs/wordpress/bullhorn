# Bullhorn

This folder contains translation files for Bullhorn.

Do not store custom translations in this folder, they will be deleted on updates.
Store custom translations in `wp-content/languages/bullhorn`.
