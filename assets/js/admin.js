/*global jQuery, document, wp, confirm*/
jQuery(document).ready(function ($) {
    'use strict';

    var __ = wp.i18n.__;

    /**
     * Expiration meta box
     */
    var Bullhorn_Expiration_Meta_Box = {
        init: function() {
            this.timestamp();
            this.tooltip();
            this.metaboxTabs();
            this.notificationType();
        },
        timestamp: function() {
            if ($('#expiration-timestamp').length) {
                var stamp = $('#expiration-timestamp').html();
                var timestampdiv = $('#bullhorn-expiration-wrap');
                var updateExpiration;

                /**
                 * Make sure all labels represent the current settings.
                 *
                 * @ignore
                 * @return {boolean} False when an invalid timestamp has been selected, otherwise True.
                 */
                updateExpiration = function() {
                    var attemptedDate, originalDate, currentDate, expireOn, aa = $('#expiration-aa').val(),
                        mm = $('#expiration-mm').val(), jj = $('#expiration-jj').val(), hh = $('#expiration-hh').val(), mn = $('#expiration-mn').val();

                    currentDate = new Date();
                    attemptedDate = new Date( aa, mm - 1, jj, hh, mn );
                    originalDate = new Date(
                        $('#hidden-expiration-aa').val(),
                        $('#hidden-expiration-mm').val() -1,
                        $('#hidden-expiration-jj').val(),
                        $('#hidden-expiration-hh').val(),
                        $('#hidden-expiration-mn').val()
                    );

                    // Catch unexpected date problems.
                    if (
                        attemptedDate.getFullYear() !== parseInt( aa, 0 ) ||
                        (1 + attemptedDate.getMonth()) !== parseInt( mm, 0 ) ||
                        attemptedDate.getDate() !== parseInt( jj, 0 ) ||
                        attemptedDate.getMinutes() !== parseInt( mn, 0 )
                    ) {
                        timestampdiv.find('.expiration-timestamp-wrap').addClass('form-invalid');
                        return false;
                    } else {
                        timestampdiv.find('.expiration-timestamp-wrap').removeClass('form-invalid');
                    }

                    // Determine what the expiration should be depending on the date.
                    if ( attemptedDate >= currentDate ) {
                        expireOn = __( 'Expires on:', 'bullhorn' );
                    } else if ( attemptedDate < currentDate ) {
                        expireOn = __( 'Expired on:', 'bullhorn' );
                    }

                    // If the date is the same, set it to trigger update events.
                    if ( originalDate.toUTCString() === attemptedDate.toUTCString() ) {
                        // Re-set to the current value.
                        $('#expiration-timestamp').html(stamp);
                    } else {
                        $('#expiration-timestamp').html(
                            '\n' + expireOn + ' <b>' +
                            // translators: 1: Month, 2: Day, 3: Year, 4: Hour, 5: Minute.
                            __('%1$s %2$s, %3$s at %4$s:%5$s')
                                .replace('%1$s', $('option[value="' + mm + '"]', '#expiration-mm').attr('data-text') )
                                .replace('%2$s', parseInt(jj, 10))
                                .replace('%3$s', aa)
                                .replace('%4$s', ('00' + hh).slice(-2))
                                .replace('%5$s', ('00' + mn).slice(-2)) +
                                '</b> '
                        );

                        $('#post-expiration').val(attemptedDate.toUTCString());
                    }

                    return true;
                };

                // Edit expiration time click.
                timestampdiv.siblings('a.edit-expiration-timestamp').on('click', function(event) {
                    if (timestampdiv.is(':hidden')) {
                        timestampdiv.slideDown('fast', function() {
                            $('input, select', timestampdiv.find('.expiration-timestamp-wrap')).first().trigger('focus');
                        } );
                        $(this).hide();
                    }
                    event.preventDefault();
                });

                // Cancel editing the expiration time and hide the settings.
                timestampdiv.find('.cancel-expiration-timestamp').on('click', function(event) {
                    timestampdiv.slideUp('fast').siblings('a.edit-expiration-timestamp').show().trigger('focus');
                    $('#expiration-mm').val($('#hidden-expiration-mm').val());
                    $('#expiration-jj').val($('#hidden-expiration-jj').val());
                    $('#expiration-aa').val($('#hidden-expiration-aa').val());
                    $('#expiration-hh').val($('#hidden-expiration-hh').val());
                    $('#expiration-mn').val($('#hidden-expiration-mn').val());
                    updateExpiration();
                    event.preventDefault();
                });

                // Remove the expiration time and hide the settings.
                timestampdiv.find('.remove-expiration-timestamp').on('click', function(event) {
                    timestampdiv.slideUp('fast').siblings('a.edit-expiration-timestamp').show().trigger('focus');
                    $('#expiration-timestamp').html(__('Expires:', 'bullhorn') + ' <b>' + __('Never', 'bullhorn') + '</b>');
                    $('#post-expiration').val('');
                    event.preventDefault();
                });

                // Save the changed timestamp.
                timestampdiv.find('.save-expiration-timestamp').on('click', function(event) {
                    if (updateExpiration()) {
                        timestampdiv.slideUp('fast');
                        timestampdiv.siblings('a.edit-expiration-timestamp').show().trigger('focus');
                    }
                    event.preventDefault();
                });

                // Cancel submit when an invalid timestamp has been selected.
                $('#post').on('submit', function(event) {
                    if ($('#post-expiration').val() && ! updateExpiration()) {
                        event.preventDefault();
                        timestampdiv.show();

                        if (wp.autosave) {
                            wp.autosave.enableButtons();
                        }

                        $('#publishing-action .spinner').removeClass('is-active');
                    }
                });
            }
        },
        tooltip: function() {
            var tooltips = $('.bullhorn-help-tip');

            bullhorn_attach_tooltips(tooltips);
        },
        metaboxTabs: function() {
            if ($('.bullhorn-metabox-tabs').length) {
                $('.bullhorn-metabox-tabs').each(function(rowIndex) {
                    var tabset = $(this);

                    tabset.tabs().addClass('ui-tabs-vertical ui-helper-clearfix');
                    tabset.find('li').removeClass('ui-corner-top').addClass('ui-corner-left');
                });
            }
        },
        notificationType: function() {
            if ($('#bullhorn-notification-type').length) {
                $('#bullhorn-notification-type').change(function() {
                    if ('image' === $(this).val()) {
                        console.log('image');
                        $('#bullhorn-notification-url-wrap').fadeIn();
                        $('#bullhorn-notification-content-wrap').fadeOut();
                    } else {
                        $('#bullhorn-notification-url-wrap').fadeOut();
                        $('#bullhorn-notification-content-wrap').fadeIn();
                    }
                }).change();
            }
        }
    };
    Bullhorn_Expiration_Meta_Box.init();

    function bullhorn_attach_tooltips(selector) {
        selector.tooltip({
            content: function() {
                return $(this).prop('title');
            },
            tooltipClass: 'bullhorn-ui-tooltip',
            position: {
                my: 'center top',
                at: 'center bottom+10',
                collision: 'flipfit'
            },
            hide: {
                duration: 200
            },
            show: {
                duration: 200
            }
        });
    }
});