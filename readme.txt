=== Bullhorn ===
Contributors: evertiro
Tags: bullhorn, widgit, notification, alert, popup, banner
Requires at least: 5.1
Tested up to: 5.7
Requires PHP: 7.2
Stable Tag: 1.0.0
License: GPLv3
License URI: http://www.gnu.org/licenses/gpl-3.0.html

Notifications simplified.

== Description ==

Simple notification management. TODO: Improve on this description before
Bullhorn goes live!

== Installation ==

Bullhorn is specifically designed to work in any WordPress-based environment.
Once downloaded, extract the `Bullhorn` directory, upload it to your
`wp-content/plugins` directory, and activate the plugin.

If you want Bullhorn to be treated as a must-use plugin, upload the `Bullhorn`
directory to your `wp-content/mu-plugins` directory; creating `mu-plugins` if
necessary. Once uploaded, copy the `mu/class-bullhorn-loader.php` file to the
`mu-plugins` directory.

== Changelog ==

## [1.0.0] - TBD
### Added
- First official release!