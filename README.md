# Bullhorn

[![License: GPL-3.0+](https://img.shields.io/badge/license-GPL--3.0%2B-blue.svg)](https://gitlab.com/widgitlabs/wordpress/bullhorn/blob/master/license.txt)
[![Pipelines](https://gitlab.com/widgitlabs/wordpress/bullhorn/badges/master/pipeline.svg)](https://gitlab.com/widgitlabs/wordpress/bullhorn/pipelines)
[![Discord](https://img.shields.io/discord/586467473503813633?color=899AF9)](https://discord.gg/jrydFBP)

The core of the Widgit platform.

## Description

Bullhorn is the core of the Widgit platform. It provides extended logging and
debugging functionality, platform-wide helper functions, Widgit-specific UI
adjustjments, and the like. Bullhorn is a must-use plugin within the Widgit
hosting environment, but can also be installed on any standard WordPress
site to add functionality to self-hosted sites. In a self-hosted environment,
each of the provided modules can be enabled at your discretion. Official Widgit
plugins may use Bullhorn-specific helper functions, but will always provide a
fallback, allowing the plugin to work in a non-Bullhorn environment.

## Installation

Bullhorn is specifically designed to work in any WordPress-based environment.
Once downloaded, extract the `Bullhorn` directory, upload it to your
`wp-content/plugins` directory, and activate the plugin.

If you want Bullhorn to be treated as a must-use plugin, upload the `Bullhorn`
directory to your `wp-content/mu-plugins` directory; creating `mu-plugins` if
necessary. Once uploaded, copy or symlink the `class-bullhorn-loader.php` file
to the `mu-plugins` directory.

## Bugs

If you find an issue, let us know [here](https://gitlab.com/widgitlabs/wordpress/bullhorn/issues)!
