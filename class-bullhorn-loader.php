<?php
/**
 * Plugin Name:     Bullhorn
 * Plugin URI:      https://widgit.io
 * Description:     Notifications simplified.
 * Author:          Widgit Team
 * Author URI:      https://widgit.io
 * Version:         1.0.0
 * Text Domain:     bullhorn
 * Domain Path:     languages
 *
 * @package         Widgit\Bullhorn\Bootstrap
 * @author          Dan Griffiths <dgriffiths@widgit.io>
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


// Conditionally load the library.
if ( ! class_exists( 'Bullhorn' ) ) {
	if ( file_exists( 'class-bullhorn.php' ) ) {
		require_once 'class-bullhorn.php';
	} elseif ( file_exists( trailingslashit( dirname( __FILE__ ) ) . '../bullhorn/class-bullhorn.php' ) ) {
		require_once trailingslashit( dirname( __FILE__ ) ) . '../bullhorn/class-bullhorn.php';
	}
}
