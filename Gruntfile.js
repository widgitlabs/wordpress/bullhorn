/*global pot*/

/**
 * Grunt config
 *
 * @since       1.0.0
 * @param       {object} grunt The grunt object
 */
module.exports = function ( grunt ) {

	// Load multiple grunt tasks using globbing patterns
	require( 'load-grunt-tasks' )( grunt );

	// Project configuration.
	grunt.initConfig( {
		pkg: grunt.file.readJSON( 'package.json' ),

		/**
		 * Minify CSS
		 *
		 * @since       1.0.0
		 * @see         {@link https://www.npmjs.com/package/grunt-contrib-cssmin|grunt-contrib-cssmin}
		 */
		cssmin: {
			options: {
				mergeIntoShorthands: false,
			},
			target: {
				files: [
					{
						expand: true,
						cwd: 'assets/css',
						src: [ '*.css', '!select2*.css' ],
						dest: 'assets/css',
						ext: '.min.css',
					},
				],
			},
		},

		/**
		 * Minify JS
		 *
		 * @since       1.0.0
		 * @see         {@link https://www.npmjs.com/package/grunt-contrib-uglify|grunt-contrib-uglify}
		 */
		uglify: {
			options: {
				mangle: false,
			},
			target: {
				files: [
					{
						expand: true,
						cwd: 'assets/js',
						src: [ '*.js', '!*.min.js', '!*jquery*.js', '!select2*.js' ],
						dest: 'assets/js',
						ext: '.min.js',
						extDot: 'last',
					},
				],
			},
		},

		/**
		 * Update pot files
		 *
		 * @since       1.0.0
		 * @see         {@link https://www.npmjs.com/package/makepot|makepot}
		 */
		makepot: {
			target: {
				options: {
					domainPath: '/languages/',                      // Where to save the POT file.
					exclude: [ 'out/.*', 'includes/libraries/.*' ], // Exclude in processing.
					mainFile: 'class-bullhorn.php',                   // Main project file.
					potFilename: 'bullhorn.pot',                      // Name of the POT file.
					potHeaders: {
						poedit: true,                               // Includes common Poedit headers.
						'x-poedit-keywordslist': true,              // Include a list of all possible gettext functions.
					},
					type: 'wp-plugin',                              // Type of project (wp-plugin or wp-theme).
					updateTimestamp: true,                          // Whether the POT-Creation-Date should be updated without other changes.
					processPot: processPot( pot ),
				},
			},
		},
	} );

	// Build task(s).
	grunt.registerTask( 'stage', [ 'cssmin', 'uglify', 'makepot' ] );
};

/**
 * Process pot files
 *
 * @since       1.0.0
 * @param       {object} pot The pot file
 * @returns     {object} pot The pot file
 */
function processPot( pot ) {
	pot.headers[ 'report-msgid-bugs-to' ] = 'https://gitlab.com/widgitlabs/wordpress/bullhorn/issues';
	pot.headers[ 'last-translator' ]      = 'Daniel J Griffiths (https://evertiro.com/)';
	pot.headers[ 'language-team' ]        = 'Widgit Labs <support@widgit.io>';
	pot.headers.language                  = 'en_US';

	let translation;

	// Exclude meta data from pot.
	// eslint-disable-next-line camelcase
	const excluded_meta = [
		'Plugin Name of the plugin/theme',
		'Plugin URI of the plugin/theme',
		'Author of the plugin/theme',
		'Author URI of the plugin/theme',
	];

	for ( translation in pot.translations[ '' ] ) {
		if ( typeof pot.translations[ '' ][ translation ].comments.extracted !== 'undefined' ) {
			if (
				// eslint-disable-next-line camelcase
				excluded_meta.indexOf(
					pot.translations[ '' ][ translation ].comments.extracted
				) >= 0
			) {
				// TODO: I think I just copy/pasted this from another plugin.
				// I really should make sure I know what it's doing and/or
				// find a better way to handle pot generation.
				// eslint-disable-next-line no-console
				console.log(
					'Excluded meta: ' +
pot.translations[ '' ][ translation ].comments.extracted
				);
				delete pot.translations[ '' ][ translation ];
			}
		}
	}
	return pot;
}
